<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/user', [UserController::class, 'index']);

Route::get('/user/create', [UserController::class, 'create']);

Route::get('/role', [RoleController::class, 'index']);

Route::get('/role/create', [RoleController::class, 'create']);
