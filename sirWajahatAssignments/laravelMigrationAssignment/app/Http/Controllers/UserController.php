<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        echo "Hello from User Controller Index";
    }
    public function create(){
        echo "Hello from User Controller Create";
        $user = new User();
        $user->username = "admin1";
        $user->password = hash('sha256',"admin123");
        $user->email = "admin@gmail.com";
        $user->status = 1;
        $user->save();
        dd($user);
    }
}
