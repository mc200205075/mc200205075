<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    public function index(){
        echo "Hello from Role Controller Index";
    }
    public function create(){
        echo "Hello from Role Controller Create";
        $role = new Role();
        $role->role_name = 'Admin';
        $role->save();
        unset($role);
        $role = new Role();
        $role->role_name = 'Teacher';
        $role->save();
        unset($role);
        $role = new Role();
        $role->role_name = 'Student';
        $role->save();
        unset($role);

        //unsetting it because in phpmyadmin sometimes all entries are saved sometimes only one last entry is saved.


    }
}
