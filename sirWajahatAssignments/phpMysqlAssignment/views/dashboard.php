<!-- ---
layout: examples
title: Dashboard Template
extra_css:
    - "dashboard.css"
extra_js:
    - src: "https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
    integrity: "sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
    - src: "https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
    integrity: "sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
    - src: "dashboard.js"
--- -->
<!DOCTYPE html>
<html lang="en">
    <?php include('dashboard/head.php'); ?>
<body>
    <?php include('dashboard/header.php'); ?> 

  <div class="container-fluid">
    <div class="row">
      <?php include('dashboard/nav.php'); ?>

      <?php include('dashboard/main.php'); ?>
    </div>
  </div>
</body>
</html>

