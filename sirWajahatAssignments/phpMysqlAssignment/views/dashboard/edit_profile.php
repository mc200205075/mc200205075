  <form action="<?php echo $base_url; ?>index.php?page=updateProfile" method="POST">

    <div class="form-floating">
      <input type="text" class="form-control" id="floatingInput" name="firstname" placeholder="First Name" value='<?php if(isset($firstname)){ echo $firstname;} ?>'>
      <label for="floatingInput">First Name</label>
    </div>
    <div class="form-floating">
      <input type="text" class="form-control" id="floatingInput" name="lastname" placeholder="Last Name" value='<?php if(isset($lastname)){ echo $lastname;} ?>'>
      <label for="floatingInput">Last Name</label>
    </div>
    <div class="form-floating">
      <input type="number" class="form-control" id="floatingInput" name="credit_card_number" placeholder="Credit Card Number" value='<?php if(isset($creditCard)){ echo $creditCard;} ?>'>
      <label for="floatingInput">Credit Card Number</label>
    </div>
    <div class="form-floating">
      <input type="text" class="form-control" id="floatingPassword" name="security_question" placeholder="Security Question" value='<?php if(isset($securityQuestion)){ echo $securityQuestion;} ?>'>
      <label for="floatingPassword">Security Question</label>
    </div>
    <div class="form-floating">
      <input type="text" class="form-control" id="floatingPassword" name="shipping_address" placeholder="Address" value='<?php if(isset($shippingAddress)){ echo $shippingAddress;} ?>'>
      <label for="floatingPassword">Address</label>
    </div>
    <input class="w-100 btn btn-lg btn-primary" type="submit" value="Sign in">
    <!-- <p class="mt-5 mb-3 text-muted">&copy; 2017–{{< year >}}</p> -->
  </form>