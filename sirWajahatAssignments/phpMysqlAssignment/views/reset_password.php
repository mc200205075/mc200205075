<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/signin.css">
  <title>Document</title>
</head>
<body class="text-center">
  <main class="form-signin w-100 m-auto">
  <form action="<?php echo $base_url; ?>index.php?page=resetPassword" method="POST">
    <img class="mb-4" src="assets/img/bootstrap-logo.svg" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Enter your Password</h1>
    <div class="form-floating">
      <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password">
      <label for="floatingPassword">Password</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="floatingPassword" name="confirm_password" placeholder="Confirm Password">
      <label for="floatingPassword">Confirm Password</label>
    </div>
    <input type="hidden" name="username" value=>
    <input class="w-100 btn btn-lg btn-primary" type="submit" value="Reset Password">
    <!-- <p class="mt-5 mb-3 text-muted">&copy; 2017–{{< year >}}</p> -->
  </form>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>

