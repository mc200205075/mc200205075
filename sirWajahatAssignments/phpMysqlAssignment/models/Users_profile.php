<?php
class Users_profile{
    public $id;
    public $user_id;
    public $firstname;
    public $lastname;
    public $credit_card_number;
    public $security_question;
    public $shipping_address;
    public $status;
    public $created_at;
    public $updated_at;

    public function setId($id){
        $this->id = $id;
    }
    public function setUserId($user_id){
        $this->user_id = $user_id;
    }
    public function setFirstName($firstname){
        $this->firstname = $firstname;
    }
    public function setLastName($lastname){
        $this->lastname = $lastname;
    }
    public function setCreditCardNumber($credit_card_number){
        $this->credit_card_number = $credit_card_number;
    }
    public function setSecurityQuestion($security_question){
        $this->security_question = $security_question;
    }
    public function setShippingAddress($shipping_address){
        $this->shipping_address = $shipping_address;
    }
    public function setStatus($status){
        $this->status = $status;
    }
    public function setCreatedAt($created_at){
        $this->created_at = $created_at;
    }
    public function setUpdatedAt($updated_at){
        $this->updated_at = $updated_at;
    }
    public function getId(){
        return $this->id;
    }
    public function getUserId(){
        return $this->user_id;
    }
    public function getFirstName(){
        return $this->firstname;
    }
    public function getLastName(){
        return $this->lastname;
    }
    public function getCreditCardNumber(){
        return $this->credit_card_number;
    }
    public function getSecurityQuestion(){
        return $this->security_question;
    }
    public function getShippingAddress(){
        return $this->shipping_address;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getCreatedAt(){
        return $this->created_at;
    }
    public function getUpdatedAt(){
        return $this->updated_at;
    }
    public function connect(){
        $database = new DB();
        return $database->connect();
    }
    public function getAll(){
        $db = $this->connect();
        $result = $db->query('SELECT * FROM users_profile WHERE status = 1');
        $i = 0;
        foreach($result as $row){
            $array[$i] = $row;
            ++$i;
        }
        return $array;
    }
    public function set($userID, $firstname, $lastname, $credit_card_number, $security_question, $shipping_address, $status){
        $this->setUserId($userID);
        $this->setFirstName($firstname);
        $this->setLastName($lastname);
        $this->setCreditCardNumber($credit_card_number);
        $this->setSecurityQuestion($security_question);
        $this->setShippingAddress($shipping_address);
        $this->setStatus($status);
    }
    public function find($user_id){
        $db = $this->connect();
        $query = "SELECT * FROM users_profile WHERE user_id = ? LIMIT 1";
        $mystat = $db->prepare($query);
        $mystat->bind_param('i', $param);
        $param = $user_id;

        $mystat->execute();
        $result = $mystat->get_result();
        
        
        return $result->fetch_object();
    }
    public function update(){
        $db = $this->connect();
        $query = "UPDATE users_profile SET firstname = ?, lastname = ?, credit_card_number = ?, security_question = ?, shipping_address = ?, status = ? WHERE user_id = ? LIMIT 1";
        $mystat = $db->prepare($query);
        $mystat->bind_param('ssissii', $firstName, $lastName, $credit_card_number, $security_question, $shipping_address, $status, $userId);
        $firstName = $this->firstname;
        $lastName = $this->lastname;
        $credit_card_number = $this->credit_card_number;
        $security_question = $this->security_question;
        $shipping_address = $this->shipping_address;
        $status = $this->status; 
        $userId = $this->user_id;

        return $mystat->execute();
    }
    public function insert(){
        $db = $this->connect();
        $query = "INSERT INTO users_profile(user_id, firstname, lastname, credit_card_number, security_question, shipping_address, status) VALUES(?, ?, ?, ?, ?, ?, ?)";
        
        $mystat = $db->prepare($query);
        
        $mystat->bind_param('ississi', $userId, $firstName, $lastName, $credit_card_number, $security_question, $shipping_address, $status,);
        
        $firstName = $this->firstname;
        $lastName = $this->lastname;
        $credit_card_number = $this->credit_card_number;
        $security_question = $this->security_question;
        $shipping_address = $this->shipping_address;
        $status = $this->status; 
        $userId = $this->user_id;
        $mystat->execute();
        echo "<pre>";
        print_r($mystat); exit;
        return $mystat->execute();
    }
}