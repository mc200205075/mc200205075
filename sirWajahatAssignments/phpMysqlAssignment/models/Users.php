<?php
class Users{
    public $id;
    public $username;
    public $password;
    public $level;
    public $status;
    public $created_at;
    public $updated_at;
    
    public function __construct($username = FALSE, $password = FALSE, $level = FALSE, $status = FALSE){
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setLevel($level);
        if($this->level != 3){
            $status = 1;
        }
        $this->setStatus($status);
    }
    public function setId($id){
        $this->id = $id;
    }
    public function setUsername($username){
        $this->username = $username;
    }
    public function setPassword($password){
        $this->password = hash('sha256',$password);
    }
    public function setLevel($level){
        $this->level = $level;
    }
    public function setStatus($status){
        $this->status = $status;
    }
    public function setCreated_at($created_at){
        $this->created_at = $created_at;
    }
    public function setUpdated_at($updated_at){
        $this->updated_at = $updated_at;
    }
    public function getId(){
        return $this->id;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getLevel(){
        return $this->level;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getCreated_at(){
        return $this->created_at;
    }
    public function getUpdated_at(){
        return $this->updated_at;
    }
    public function connect(){
        $database = new DB();
        return $database->connect();
    }
    public static function connectDB(){
        $database = new DB();
        return $database->connect();
    }
    public function insert(){
        $db = $this->connect();
        $stmt = $db->prepare("INSERT INTO users (username, password, status) VALUES (?, ?, ?)");
        $stmt->bind_param('ssi', $username, $password, $status);
        $username = $this->username; 
        $password = $this->password;
        $status = $this->status;
        return $stmt->execute();
    }
    public function getAll(){
        $db = $this->connect();
        $result = $db->query('SELECT * FROM users');
        return $result->fetch_object();
    }
    public function findId($id){
        $db = $this->connect();
        $result = $db->query('SELECT * FROM users WHERE id = '.$id);
        return $result->fetch_object();
    }
    public static function findUser($username){
        $db = Users::connectDB();
        $result = $db->query("SELECT * FROM users WHERE username = '".$username."'");
        // print_r($result->fetch_object());
        return $result->fetch_object();
    }
    public static function findVerifiedUser($username){
        $db = Users::connectDB();
        $result = $db->query("SELECT * FROM users WHERE username = '".$username."' AND status = 1");
        // print_r($result->fetch_object());
        return $result->fetch_object();
    }
    public function insert_into_DB(){
        $this->insert();
        $user = $this->findUser($this->username);
        // print_r($user); exit;
        $m2m_level = new M2m_users_level($user->id, $this->level);
        // print_r($m2m_level);
        return $m2m_level->insert();
    }
    public function update(){
        $db = $this->connect();
        $query = "UPDATE users SET username = ?, password = ?, level = ?, status = ?us = ? WHERE user_id = ? LIMIT 1";
        $mystat = $db->prepare($query);
        $mystat->bind_param('ssissii', $firstName, $lastName, $credit_card_number, $security_question, $shipping_address, $status, $userId);
        $firstName = $this->firstname;
        $lastName = $this->lastname;
        $credit_card_number = $this->credit_card_number;
        $security_question = $this->security_question;
        $shipping_address = $this->shipping_address;
        $status = $this->status; 
        $userId = $this->user_id;

        return $mystat->execute();
    }
    
}