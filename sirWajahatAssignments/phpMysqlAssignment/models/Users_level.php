<?php
class Users_level{
    public $id;
    public $name;
    public $status;
    public $created_at;
    public $updated_at;

    public function __construct($id = FALSE, $name = FALSE, $status = FALSE, $created_at = FALSE, $updated_at = FALSE){
        if(isset($id)) $this->setId($id);
        if(isset($name)) $this->setName($name);
        if(isset($status)) $this->setStatus($status);
        if(isset($created_at)) $this->setCreated_at($created_at);
        if(isset($updated_at)) $this->setUpdated_at($updated_at);
    }

    public function setId($id){
        $this->id = $id;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function setStatus($status){
        $this->status = $status;
    }
    public function setCreated_at($created_at){
        $this->created_at = $created_at;
    }
    public function setUpdated_at($updated_at){
        $this->updated_at = $updated_at;
    }
    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getCreated_at(){
        return $this->created_at;
    }
    public function getUpdated_at(){
        return $this->updated_at;
    }
    public function connect(){
        $database = new DB();
        return $database->connect();
    }
    public function getAll(){
        $db = $this->connect();
        $result = $db->query('SELECT * FROM users_level WHERE status = 1');
        $i = 0;
        foreach($result as $row){
            $array[$i] = $row;
            ++$i;
        }
        return $array;
    }
    public function find($role_id){
        $db = $this->connect();
        $result = $db->prepare("SELECT * FROM users_level WHERE id = ?");
        $result->bind_param('i', $roleId);

        $roleId = $role_id;

        $result->execute();        

        // print_r($result->fetch_object());
        return $result->get_result()->fetch_object();
    }
}