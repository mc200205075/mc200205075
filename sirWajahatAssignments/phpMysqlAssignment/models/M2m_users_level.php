<?php 
class m2m_users_level{
    public $user_id;
    public $level_id;

    public function __construct($user_id = FALSE, $level_id = FALSE){
        $this->setUserId($user_id);
        $this->setLevelId($level_id);
    }
    public function setUserId($user_id){
        $this->user_id = $user_id;
    }
    public function setLevelId($level_id){
        $this->level_id = $level_id;
    }

    public function getUserId(){
        return $this->user_id;
    }
    public function getLevelId(){
        return $this->level_id;
    }
    public function findLevel($user_id){
        $db = $this->connect();
        $result = $db->query("SELECT * FROM m2m_users_level WHERE user_id = '".$user_id."'");
        // print_r($result->fetch_object());
        return $result->fetch_object();
    }
    public function connect(){
        $database = new DB();
        return $database->connect();
    }
    public function insert(){
        $db = $this->connect();
        $stmt = $db->prepare("INSERT INTO m2m_users_level (user_id, level_id) VALUES (?, ?)");
        $stmt->bind_param('ii', $user_id, $level_id);
        $user_id = $this->user_id; 
        $level_id = $this->level_id;
        return $stmt->execute();
    }
}