<?php 
class register{
    public $root;
    public $base_url;

    public static function signup(){
        // print_r($_GET);
        // print_r($_POST);
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";  
        $signedup = $_GET['page'] == 'signedup';
        if($_POST && $signedup){
            $userName = $_POST['username'];
            $password = $_POST['password'];
            $level = $_POST['level'];

            $validatedEmail = filter_var($userName, FILTER_VALIDATE_EMAIL);
            $validatedMobile = is_numeric($userName) && strlen($userName) == 11;
            $validatedLevel = is_numeric($level) && strlen($level) == 1;

            $validatedUsername = $validatedEmail || $validatedMobile;
            $validatedPassword = strlen($password) < 15;
            
            $validatedInput = $validatedUsername && $validatedPassword && $validatedLevel;  
            
            if($validatedInput){
                register::authenticate();
            }
            else{
                echo "Username and Password are not valid"; exit;
            }
        }
        

        // $user->insert();
    }
    public static function authenticate(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";  
        $username = $_POST['username'];
        $password = $_POST['password'];
        $level = $_POST['level'];
        $user = new Users($username, $password, $level);
        $searchedUser = Users::findUser($username);
        // print_r($searchedUser); exit;
        if($searchedUser){
            echo "It seems the username you provided already exists"; exit; 
        }
        else{
            if($user->insert_into_DB()){
                echo "<div class='text-center'>User Registered Successfully</div>";
                include($root.'/views/login.php');
            }
            else{
                main::register();
            }
        }
        
    }
}