<?php

class main{
    public $root;
    public $base_url;
    public static function login(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        if(!$_SESSION){                
            include($root.'/views/login.php');
        }       
        exit;
    }
    public static function register(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        $users_level = new Users_level();
        $users_level = $users_level->getAll();
        // print_r($_SESSION); exit;
        if(!$_SESSION){        
            // echo "register "; exit;
            include($root.'/views/register.php');
        }
        else{
            header("Location: ".$base_url."index.php?page=dashboard"); 
        }
        exit;
    }
    public static function profile(){
        if($_SESSION){
            $_SESSION['page'] = 'profile';

            dashboard::profile();
        }
    }
    public static function editProfile(){
        if($_SESSION){
            $_SESSION['page'] = $_GET['page'];

            dashboard::editProfile();
        }
    }
    public static function updateProfile(){
        if($_SESSION){
            $_SESSION['page'] = $_GET['page'];

            dashboard::updateProfile();
        }
    }
    public static function forgotPassword(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        if(!$_SESSION){                
            include($root.'/views/forgot_password.php');
        }       
        exit;
    }
    public static function dashboard(){
        if($_SESSION){
            $_SESSION['page'] = $_GET['page'];
            dashboard::loggedin();
        }
    }
    public static function medicine(){
        if($_SESSION){
            $_SESSION['page'] = $_GET['page'];
            pharmacist::medicine();
        }
    }
    public static function validate(){
        $login = $_GET['page'] == 'loggedin';
        
        if($_POST && $login){
            $userName = $_POST['username'];
            $password = $_POST['password'];

            $validatedEmail = filter_var($userName, FILTER_VALIDATE_EMAIL);
            $validatedMobile = is_numeric($userName) && strlen($userName) == 11;

            $validatedUsername = $validatedEmail || $validatedMobile;
            $validatedPassword = strlen($password) < 15;
            
            $validatedInput = $validatedUsername && $validatedPassword;  
            
            if($validatedInput){
                main::authenticate();
            }
            else{
                echo "Incorrect Username or Password"; exit;
            }

            
        }
    }
    public static function authenticate(){
        if($_POST && $_GET['page'] == 'loggedin'){
            $loginUser = new Users($_POST['username'], $_POST['password']);
            $isRegistered = Users::findUser($_POST['username']); 
            
            if(isset($isRegistered->id)){
                $m2m_users_level = new M2m_users_level();
                $level = $m2m_users_level->findLevel($isRegistered->id);
            }
            if(isset($level)){
                if(isset($level->level_id)){
                    $users_level = new Users_level();
                    $levelUser = $users_level->find($level->level_id);
                    // echo "<pre>"; print_r($levelUser); exit;
                }
                 
            }
            // // print_r($level); exit;
            // echo "<pre>";
            // print_r($loginUser);
            // print_r($isRegistered);
            // exit; 
            if(!$isRegistered){
                echo "You are not registered with the system";
            }
            else{
            
                $loginPassword = $loginUser->password;
                $registeredPassword = $isRegistered->password;
                $passwordMatch = $loginPassword == $registeredPassword;
                $userStatus = $isRegistered->status == 1;
                $isAdmin = $level->level_id == 3;
                if($isAdmin && !$userStatus){
                    echo "You need to get validated before using Admin Account"; exit;
                }
                if(!$passwordMatch){
                    echo "It seems you forgot your password";
                }
                else{
                    if($_POST){                        
                        $_SESSION['page'] = $_GET['page'];
                        $_SESSION['user_id'] = $isRegistered->id;
                        $_SESSION['username'] = $isRegistered->username;
                        $_SESSION['status'] = $isRegistered->status;
                        $_SESSION['level'] = $level->level_id;
                        if(isset($levelUser->name)){
                            $_SESSION['role'] = $levelUser->name;
                        }   
                       
                        // echo "<pre>"; print_r($_SESSION); exit;
                    }
                    if($_SESSION){
                        dashboard::loggedIn();
                    }
                    
                }
            }
        
        }
        //unset($users, $loginUser);
        
    }
    public static function logout(){  
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        if(isset($_SESSION)){
            if($_SESSION){
                session_destroy();
            }
            
        }
        header("Location: ".$base_url."index.php?page=login");       
    }
    public static function searchUsername(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        if($_POST){
            $username = $_POST['username'];
        }
        if(isset($username)){
                $user = new Users();
                $profile = $user->findUser($username);
                if(isset($profile)){
                    include($root.'/views/reset_password.php');
                }
                else{
                    echo "The username you entered does not exist in the system";
                }
                
        }
        else {
            echo "You have not entered correct username";  
            echo "<br><a href='".$base_url."index.php?page=forgotPassword'>Back to the Forgot Password Page</a>";
            
        }
       
    }
    public static function resetPassword(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/"; 
        if($_POST){
            echo "<pre>";
            $password = hash('sha256',$_POST['password']);
            $confirm_password = hash('sha256',$_POST['confirm_password']);

            if($password == $confirm_password){
                
            }
            else{
                    echo "The password you entered does not match";
                    echo "<br><a href='".$base_url."index.php?page=forgotPassword'>Back to the Forgot Password Page</a>";
            }
        }
        if(isset($username)){
                $user = new Users();
                $profile = $user->findUser($username);
                if(isset($profile)){
                    include($root.'/views/reset_password.php');
                }
                else{
                    echo "The username you entered does not exist in the system";
                }
                
        }
    }
}