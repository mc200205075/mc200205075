<?php 

class dashboard{
    public $base_url;
    public $root;
    public $page;
    public static function __constructStatic(){
        
    }
    public static function loggedIn(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";        
        if(isset($_SESSION)){
            // echo "Logged In! ";
            $page = $_SESSION['page'];
            $role = $_SESSION['role']; 
            $user_id = $_SESSION['user_id'];
        //    echo "Welcome to the ".$user." Panel<br>";
        //    echo "<a href='index.php/logout'>Logout</a>";
           include($root.'/views/dashboard.php');
       }
    }
    public static function profile(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";   
        if(isset($_SESSION)){
            // echo "Logged In! ";
            $page = $_SESSION['page'];
            $role = $_SESSION['role']; 
            $user_id = $_SESSION['user_id'];
            $users_profile = new Users_profile();
            $users_profile = $users_profile->find($user_id);  
            if(isset($users_profile)){
                $name = $users_profile->firstname." ".$users_profile->lastname;
                $creditCard = $users_profile->credit_card_number;
                $securityQuestion = $users_profile->security_question;
                $shippingAddress = $users_profile->shipping_address;
            }            
            include($root.'/views/dashboard.php');
        }
    }
    public static function editProfile(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";   
        if(isset($_SESSION)){
            // echo "Logged In! ";
            $page = $_SESSION['page'];
            $role = $_SESSION['role']; 
            $user_id = $_SESSION['user_id'];

            $user_find = new Users_profile();
            $users_profile = $user_find->find($user_id);
            if(isset($users_profile)){
                $firstname = $users_profile->firstname;
                $lastname = $users_profile->lastname;
                $creditCard = $users_profile->credit_card_number;
                $securityQuestion = $users_profile->security_question;
                $shippingAddress = $users_profile->shipping_address;
            }     
            
            include($root.'/views/dashboard.php');
        }
    }
    public static function updateProfile(){
        $root = dirname(__FILE__,2);
        $base_url = "http://".$_SERVER['HTTP_HOST']."/mc200205075/sirWajahatAssignments/phpMysqlAssignment/";   
        
        if(isset($_SESSION)){
            // echo "Logged In! ";
            $page = $_SESSION['page'];
            $role = $_SESSION['role']; 
            $user_id = $_SESSION['user_id'];
            if($_POST){
                $firstName = $_POST['firstname'];
                $lastName = $_POST['lastname'];
                $creditCardNumber = $_POST['credit_card_number'];
                $securityQuestion = $_POST['security_question'];
                $shippingAddress = $_POST['shipping_address'];
            }
            
            $validatedData = array();
            if(isset($firstName)){
                if(strlen($firstName) <= 50){
                    $validatedData['firstname'] = $firstName;
                }
            }
            if(isset($lastName)){
                if(strlen($lastName) <= 50){
                    $validatedData['lastname'] = $lastName;
                }
            }
            if(isset($creditCardNumber)){
                if(strlen($creditCardNumber) == 16){
                    $validatedData['credit_card_number'] = $creditCardNumber;
                }
            }
            if(isset($securityQuestion)){
                if(strlen($securityQuestion) <= 255){
                    $validatedData['security_question'] = $securityQuestion;
                }
            }
            if(isset($shippingAddress)){
                if(strlen($shippingAddress) <= 255){
                    $validatedData['shipping_address'] = $shippingAddress;
                }
            }
            
            $users_profile = new Users_profile();
            $profile = $users_profile->find($user_id);
            
            if(isset($validatedData)){
                
                if(isset($profile)){
                    $users_profile->set($user_id, $validatedData['firstname'], $validatedData['lastname'], $validatedData['credit_card_number'], $validatedData['security_question'], $validatedData['shipping_address'], 1);
                    // echo "<pre>";
                    // print_r($users_profile);exit;
                    $users_profile->update();
                }
                else{
                    $user_insert = new Users_profile();
                    $user_insert->set($user_id, $validatedData['firstname'], $validatedData['lastname'], $validatedData['credit_card_number'], $validatedData['security_question'], $validatedData['shipping_address'], 1);
                    
                    $user_insert->insert();
                    
                }
            }
            
            
            // echo "<pre>"; 
            // print_r($user);
            // print_r($validatedData); exit;
            main::profile();
        }
    }
}