<?php 

$query = 'CREATE TABLE IF NOT EXISTS users_profile(
    -- id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT(11) UNSIGNED PRIMARY KEY,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    credit_card_number BIGINT(16),
    security_question VARCHAR(255),
    shipping_address VARCHAR(255),
    status INT(1) DEFAULT 0,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(ID)
)';

$mysqli->query($query);

unset($query);