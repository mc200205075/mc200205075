<?php 

$query = 'CREATE TABLE IF NOT EXISTS m2m_users_level(
    user_id INT(11) UNSIGNED,
    level_id INT(11) UNSIGNED,
    PRIMARY KEY (user_id, level_id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (level_id) REFERENCES users_level(id)

)';

$mysqli->query($query);

unset($query);