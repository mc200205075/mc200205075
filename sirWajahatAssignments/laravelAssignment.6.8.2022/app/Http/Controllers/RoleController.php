<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    public function index(){
        echo "Hello From method index of RoleController";
        die();
    }
    public function create(){
        echo "Hello from method create of RoleController";
        $role = new Role();
        $role->role_name = 'Admin';
        $role->save();
        unset($role);

        $role1 = new Role();
        $role1->role_name = 'Teacher';
        $role1->save();
        unset($role1);

        $role3 = new Role();
        $role3->role_name = 'Student';
        $role3->save();
        unset($role3);
    }
}
