<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        echo "Hello from User Controller"; die();
    }
    public function create(){
        echo "Hello from method create of User Controller";
        $user = new User();
        $user->username = 'admin1';
        $user->password = hash('sha256', 'admin123');
        $user->email = 'admin@gmail.com';
        $user->status = 1;
        // dd($user);
        $user->save();
        unset($user);
    }
}
